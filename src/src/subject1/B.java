package subject1;

import java.util.ArrayList;

interface C
{

}

public class B implements C{
    private long t;
    private E variable;
    public void x()
    {

    }
}

class A
{
    public void met(B b)
    {
        B something;
    }
}

class D
{
  private ArrayList<B> b=new ArrayList<B>();

  public void add(B bla)
  {
      b.add(bla);
  }

}

class E
{
    private ArrayList<G> g= new ArrayList<G>();
    private F f;

    public E()
    {
        f= new F();
    }

    public void metG(int i)
    {

    }

}

class F
{
    public void metA()
    {

    }

}

class G
{

}


