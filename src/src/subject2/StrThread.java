package subject2;

import java.time.LocalDateTime;

public class StrThread implements Runnable{

    int increment = 0;

    public void run(){
        while(increment < 5) {
            System.out.println(Thread.currentThread().getName() + " current time: " + LocalDateTime.now());
            increment++;
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        StrThread StrThread1 = new StrThread();
        StrThread StrThread2 = new StrThread();

        Thread thread1 = new Thread(StrThread1);
        Thread thread2 = new Thread(StrThread2);

        thread1.start();
        thread2.start();
    }
}
